import Vue from 'vue'
import VueRouter from 'vue-router'
import index from '../views/Index.vue'
import recommend from '../views/Recommend.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/index',
  },
  {
    path: '/index',
    component: index,
    children: [
      { path: "/", redirect: "recommend" },
      {
        path: 'recommend',
        name: 'recommend',
        component: recommend
      },
      {
        path: 'ranking',
        name: 'ranking',
        component: () => import('../views/Ranking.vue')
      }
      , {
        path: 'singer',
        name: 'singer',
        component: () => import('../views/Singer.vue')
      }
    ]
  },
  { // 推荐
    path: "/recommend/playlistMore",
    name: "playlistMore",
    component: () => import("../components/recommend/playlistMore.vue")
  },
  {
    path: "/recommend/playlistMorelist",
    name: "playlistMorelist",
    component: () => import("../components/recommend/playlistMorelist.vue")
  },
  {
    path: "/recommend/playlistMoreSingle",
    name: "playlistMoreSingle",
    component: () => import("../components/recommend/playlistMoreSingle.vue")
  },
  {
    path: "/recommend/newDiscMore",
    name: "newDiscMore",
    component: () => import("../components/recommend/newDiscMore.vue")
  },
  {
    path: "/recommend/newDiscMorelist",
    name: "newDiscMorelist",
    component: () => import("../components/recommend/newDiscMorelist.vue")
  },
  {
    path: "/recommend/newDiscSingle",
    name: "newDiscSingle",
    component: () => import("../components/recommend/newDiscSingle.vue")
  },
  {
    path: "/recommend/playlistMorelist",
    name: "playlistMorelist",
    component: () => import("../components/recommend/playlistMorelist.vue")
  },

  { // 排行榜
    path: "/rank/ranklist",
    name: "ranklist",
    component: () => import("../components/rank/ranklist.vue")
  },
  { // 歌手
    path: "/singer/artists",
    name: "artists",
    component: () => import("../components/singer/artists.vue")
  },
  { // 搜索
    path: "/base/search",
    name: "search",
    component: () => import("../components/base/search.vue")
  },
  {
    path: "/search/searchList",
    name: "searchList",
    component: () => import("../components/search/searchList.vue")
  },
  { // 视频页面
    path: '/video',
    component: () => import('../views/video.vue'),
  },
  { // 歌手其他视频页面
    path: '/mvplay/mvplay',
    name: "mvplay",
    component: () => import('../components/mvplay/mvplay.vue'),
  },
  // 用户页面
  {
    path: "/userInfo",
    name: "userInfo",
    component: () => import("../views/userinfo.vue"),
    beforeEnter: (to, from, next) => {
      let login = window.localStorage.getItem("token");
      // 判断没有值
      if (!login) {
        next("/login");
        return;
      } else {
        next();
      }
    }
  },
  {
    path: "/login",
    name: "login",
    component: () => import("../components/base/login.vue")
  },
  // 关注
  {
    path: "/attention",
    name: "attention",
    component: () => import("../views/attention.vue")
  },
  // 最近播放
  {
    path: "/userInfo/recPlayed",
    name: "recPlayed",
    component: () => import("../components/userInfo/recPlayed.vue")
  },
  // 本地下载
  {
    path: "/userInfo/localDownload",
    name: "localDownload",
    component: () => import("../components/userInfo/localDownload.vue")
  },

  //已购
  {
    path: "/userInfo/purchased",
    name: "purchased",
    component: () => import("../components/userInfo/purchased.vue")
  },
  // 云盘
  {
    path: "/userInfo/cloudDisk",
    name: "cloudDisk",
    component: () => import("../components/userInfo/cloudDisk.vue")
  },
  // 我的播客
  {
    path: "/userInfo/podcast",
    name: "podcast",
    component: () => import("../components/userInfo/podcast.vue")
  },
  // 收藏和赞
  {
    path: "/userInfo/collection",
    name: "collection",
    component: () => import("../components/userInfo/collection.vue")
  },
  //广播电台
  {
    path: "/userInfo/studio",
    name: "studio",
    component: () => import("../components/userInfo/studio.vue")
  },
  {
    path: "/userInfo/singList",
    name: "singList",
    component: () => import("../components/userInfo/singList.vue")
  },
]

const router = new VueRouter({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
