import Vue from 'vue'
import Vant from 'vant'
import App from './App.vue'
import router from './router'
import store from './store'

import 'vant/lib/index.css';
Vue.use(Vant);

Vue.config.productionTip = false

// 引入字体图标
import "./assets/font/iconfont.css"

//引入swipe
import "swiper/dist/css/swiper.min.css"


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
