import request from "./http"

// 获取排行歌单
export function getToplist() {
    return request({
        url: "/toplist",
        method: "get"
    })
}

// 获取歌单详情
// /playlist/detail?id=24381616
export function getPlaylistDetail(params) {
    return request({
        url: "/playlist/detail",
        method: "get",
        params
    })
}
// 获取专辑歌单详情
// /album?id=136644109
export function getPlayalbum(params) {
    return request({
        url: "/album",
        method: "get",
        params
    })
}
