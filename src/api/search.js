import request from "./http"

// 热搜列表
export function getSearchlist(){
    return request({
        url:"/search/hot/detail",
        methods:"get",
    })
}

// 关键字
// /search/suggest?keywords= 海阔天空&type=mobile
export function getSearchKeyword(params){
    return request({
        url:"/search/suggest",
        methods:"get",
        params
    })
}

// /cloudsearch?keywords=周深
export function getCloudSearch(params){
    return request({
        url:"/cloudsearch",
        methods:"get",
        params
    })
}
