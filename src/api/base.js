import request from "./http"

//获取歌曲
export function getSongUrl(params) {
    return request({
        url: "/song/url",
        methods: "get",
        params
    })
}

// 获取歌曲详情
// /song/detail?ids=347230
export function getSongDetail(params){
    return request({
        url:"/song/detail",
        methods:"get",
        params
    })
}

// 获取歌词
// /lyric?id=1811921555
export function getLyric(params){
    return request({
        url:"/lyric",
        methods:"get",
        params
    })
}

