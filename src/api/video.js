import request from "./http"

// 全部MV
// /mv/all
export function getMvAll() {
    return request({
        url: "/mv/all",
        methods: "get",
    })
}

// 推荐MV
///personalized/mv
export function getMVCommend() {
    return request({
        url: "/personalized/mv",
        methods: "get",
    })
}

// 最新MV
// /mv/first
export function getMVFirst() {
    return request({
        url: "/mv/first",
        methods: "get",
    })
}

// 排行MV
// /top/mv
export function getMVTop() {
    return request({
        url: "/top/mv",
        methods: "get",
    })
}

// 独家MV
// /mv/exclusive/rcmd
export function getMVExclusive() {
    return request({
        url: "/mv/exclusive/rcmd",
        methods: "get",
    })
}

// MV详情
// /mv/detail?mvid=5436712
export function getMVDetail(params) {
    return request({
        url: "/mv/detail",
        methods: "get",
        params
    })
}

// 播放MV
// /mv/url?id=5436712 /mv/url?id=10896407&r=1080
export function getMVPlay(params) {
    return request({
        url: "/mv/url",
        methods: "get",
        params
    })
}

// 歌手MV
// /artist/mv?id
export function getMVSIng(params) {
    return request({
        url: "/artist/mv",
        methods: "get",
        params
    })
}
// MV评论
// /comment/mv?id=5293585
export function getMVComment(params) {
    return request({
        url: "/comment/mv",
        methods: "get",
        params
    })
}

//留言
// t:1 发送, 2 回复
// type
// 0: 歌曲
// 1: mv
// 2: 歌单
// 3: 专辑
// 4: 电台
// 5: 视频
// 6: 动态
// /comment?t=1&type=1&id=5436712&content=test
export function getTalk(params) {
    return request({
        url: "/comment",
        methods: "get",
        params
    })
}

// mvid : MV id 
// t : 1 为收藏,其他为取消收藏
// 接口地址 : /mv/sub

export function getMVAdd(params) {
    return request({
        url: "/mv/sub",
        methods: "get",
        params
    })
}
