//歌手接口
import request from "./http"

// 获取排行版歌单
// /artist/list?type=1&area=96&initial=b
export function getArtistlist(params){
    return request({
        url:"/artist/list",
        method:"get",
        params
    })
}

// 获取歌手单曲详情
// /artists?id=6452
export function getArtists(params) {
    return request({
        url: "/artists",
        method: "get",
        params
    })
}

// 歌曲评论
// /comment/music?id=186016
export function getSingerComment(params) {
    return request({
        url: "/comment/music",
        methods: "get",
        params
    })
}

// 歌曲评论
// /like?id=347230  like true/false
export function getliked(params) {
    return request({
        url: "/like",
        methods: "get",
        params
    })
}
// 点赞
///comment/like?id=29178366&cid=12840183&t=1&type=0
export function getlikeStar(params) {
    return request({
        url: "/comment/like",
        methods: "get",
        params
    })
}