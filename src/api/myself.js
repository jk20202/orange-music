import request from "./http";

//获取用户详情
// /user/detail?uid
export function getMyself(params) {
    return request({
        url: "/user/detail",
        method: "get",
        params
    })
}

// 获取用戶列表信息
// /user/playlist?uid=6331818874
export function getMyselfList(params) {
    return request({
        url: "/user/playlist",
        method: "get",
        params,
    })
}

// 数量
// /user/subcount
export function getNumMes() {
    return request({
        url: "/user/subcount",
        method: "get",
    })
}

// /user/follows?uid=6331818874
export function getFollow(params) {
    return request({
        url: "/user/follows",
        method: "get",
        params
    })
}
// /personalized/djprogram
export function getPersonalized() {
    return request({
        url: "/personalized/djprogram",
        method: "get",
    })
}

//user/record?uid=6331818874&type=1
// 最近播放歌曲 - 用户播放
export function getUserRecent(params) {
    return request({
        url: 'user/record',
        method: 'get',
        params
    })
}

//歌手收藏列表
// /artist/sublist
export function getSublist() {
    return request({
        url: "/artist/sublist",
        method: "get",
    })
}

//歌手收藏MV
// /mv/sublist
export function getMVSublist() {
    return request({
        url: "/mv/sublist",
        method: "get",
    })
}