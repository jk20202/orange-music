// 推荐接口
import request from "./http";

// 获取轮播图数据
export function getBanner(){
    return request({
        url:"/banner?type=3",
        method:"get",
    })
}

// 获取推荐歌单数据
export function getPersonalizedr(){
    return request({
        url:"/personalized?limit=6",
        method:"get",
    })
}

// 获取推荐歌单全部数据
export function getAllPersonalizedr(){
    return request({
        url:"/personalized",
        method:"get",
    })
}

// 获取新碟上架数据
export function getAlbum(){
    return request({
        url:"/album/newest",
        method:"get",
    })
}

// 获取全部新碟上架数据
export function getAlbumAll(){
    return request({
        url:"/album/list",
        method:"get",
    })
}

