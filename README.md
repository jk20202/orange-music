# 橙子音乐

# 首页

![Image text](https://gitee.com/hency-lin/orange-music/raw/master/img/%E9%A6%96%E9%A1%B5.png)



# 排行榜

![Image text](https://gitee.com/hency-lin/orange-music/raw/master/img/%E6%8E%92%E8%A1%8C%E6%A6%9C.png)

# 歌手

![Image text](https://gitee.com/hency-lin/orange-music/raw/master/img/%E6%AD%8C%E6%89%8B.png)

# 歌单

![Image text](https://gitee.com/hency-lin/orange-music/raw/master/img/%E6%AD%8C%E5%8D%95.png)



# 播放歌曲

![Image text](https://gitee.com/hency-lin/orange-music/raw/master/img/%E6%92%AD%E6%94%BE.png)

# 博客

![Image text](https://gitee.com/hency-lin/orange-music/raw/master/img/%E5%8D%9A%E5%AE%A2.png)

# 注：

我的和关注因本地接口问题无法正常使用，建议所有API接口使用网络接口

接口文档：https://apis.netstart.cn/music/#/

## Project setup

```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
