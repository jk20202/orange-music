module.exports = {
    devServer: {
        // 注意：修改当前文件重启项目
        proxy: {
            "/": {
                target: "http://localhost:3000",
                changeOrigin: true,
            }
        }
    }
}